const program = require('commander');
// Require logic.js file and extract controller functions using JS destructuring assignment
const { addContact, getContact, getContactList, deleteContact, updateContact } = require('./dbLogic');
const { prompt } = require('inquirer');

const questions = [
    {
        type: 'input',
        name: 'firstname',
        message: 'Enter firstname ..'
    },
    {
        type: 'input',
        name: 'lastname',
        message: 'Enter lastname ..'
    },
    {
        type: 'input',
        name: 'phone',
        message: 'Enter phone number ..'
    },
    {
        type: 'input',
        name: 'email',
        message: 'Enter email address ..'
    }
];

program
    .version('0.0.1')
    .description('Contact management system');

program
    .command('addContact')
    .alias('a')
    .description('Add a contact')
    .action(() => {
        prompt(questions).then((answers) =>
            addContact(answers));

    });

program
    .command('getContact <name>')
    .alias('r')
    .description('Get contact')
    .action(name => getContact(name));

program
    .command('getContactList')
    .alias('ls')
    .description('Get contactList')
    .action(list => getContactList());

program
    .command('deleteContact <id>')
    .alias('rm')
    .description('Delete given contact')
    .action(id => deleteContact(id));



program
    .command('updateContact <id> ')
    .alias('u')
    .description('Update given contact')
    .action(_id => {
        prompt(questions).then((answers) =>
            updateContact(_id, answers));
    });

// Assert that a VALID command is provided 
if (!process.argv.slice(2).length || !/[arudl]/.test(process.argv.slice(2))) {
    program.outputHelp();
    process.exit();
}


program.parse(process.argv);